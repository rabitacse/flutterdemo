import 'package:flutter/material.dart';
import 'package:test_project/menu.dart';
/*--dsdsd*/
class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: Container(),
       appBar: AppBar(
         title: Text('App Name'),
         actions: <Widget>[
           IconButton(icon: Icon(Icons.search),
           onPressed: (){
           },)
         ],
       ),
       drawer: Drawer(
         child:ListView(

           children: <Widget>
           [
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(
                color:Colors.blue,
              ),
            ),

            ListTile(
             title: Text('Item 1'),
             onTap: (){
              Navigator.pop(context);
             },

            ),

            ListTile(
              title: Text('Item 2'),
              onTap: (){
               Navigator.pop(context);
              },
            )

           ],
         ),
       ),
       floatingActionButton: FloatingActionButton(
         child: Icon(Icons.edit,),
         backgroundColor: Colors.black,
         elevation:12,
         onPressed: (){
             Navigator.of(context).push(new MaterialPageRoute(builder: (context)=> Menu()));
           print("button pressed");
         },
       ),
    );
  }
}

